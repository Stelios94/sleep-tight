﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAudio : MonoBehaviour

{

    // Variable for storing the Soundclip
    public AudioClip SoundClip;

    // Variable for storing the Soundtrack
    public AudioSource SoundTrack;


    // State of the Music toggle
    bool musicToggle;

   


    // Use this for initialization
    void Start ()
    {
        SoundTrack.clip = SoundClip;

	}
	

    // Plays the current soundtrack
    public void PlayMusic()
    {
        SoundTrack.Play();

    }


    // Stops the current soundtrack
    public void StopMusic()
    {
        SoundTrack.Stop();

    }


    // Toggles the music on or off
    public void ToggleMusic()
    {
        musicToggle = !musicToggle;

        if (musicToggle == true)
        {
            SoundTrack.volume = 0;
        }
        else
        {
            SoundTrack.volume = 100;
        }

    }





}
