﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeSelection : MonoBehaviour
{

    // Current state of the upgrade toggles
    bool hpState;
    bool dmgState;

    // Cost of the upgrades
    int hpCost;
    int dmgCost;

    // Increase amount for the upgrades
    int hpIncrease;
    int dmgIncrease;

    // Placeholder (should be in it's own teddy script)
    int teddyHp;
    int teddyDmg;


    // Text box objects
    public Text hpDescription;
    public Text dmgDescription;


    public PlayerController player;


    // Activates upon running program
    void Start()
    {

        // Description with cost and increase for upgrades
        hpDescription.text = "Cost: " + hpCost.ToString() + "\n" + "Health Increase: " + hpIncrease.ToString();
        dmgDescription.text = "Cost: " + dmgCost.ToString() + "\n" + "Damage Increase: " + dmgIncrease.ToString();

    }


    // Changes the current state
    public void UpdateHpState()
    { 
        hpState = !hpState;
    }

    // Changes the current state
    public void UpdateDmgState()
    {
        dmgState = !dmgState;
    }


    // Cancels the selection of upgrades
    public void CancelSelection()
    {
        hpState = false;
        dmgState = false;
    }



    // Adds any selected upgrades to player as well as remove from total currency
    // Checks if the player has selected the option and if they can afford the upgrade
    public void PurchaseUpgrade()
    {

        if (hpState == true)
        {
            if (player.coinAmount >= hpCost)
            {

                player.coinAmount -= hpCost;
                teddyHp += hpIncrease;

                hpState = false;

            }

        }

        if (dmgState == true)
        {
            if (player.coinAmount >= dmgCost)
            {

                player.coinAmount -= dmgCost;
                teddyDmg += dmgIncrease;

                dmgState = false;

            }

        }

        // Else display some kind of error, eg. not enough coins




    }




}
