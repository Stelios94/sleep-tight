﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    public GameObject pauseMenuUI;
    public GameObject inGameUI;
    public GameObject settingsUI;
    public GameObject upgradeUI;
    public GameObject Level1;

    public bool isShowing;

    // Use this for initialization
    void Start()
    {
        // Hides the UI
        pauseMenuUI.SetActive(isShowing);
        inGameUI.SetActive(isShowing);
        settingsUI.SetActive(isShowing);
        upgradeUI.SetActive(isShowing);
        Level1.SetActive(isShowing);


    }

    // Update is called once per frame
    void Update()
    {

   
        // If the user presses "esc", toggle the pause menu UI
        if (Input.GetKeyDown("escape"))
        {
            isShowing = !isShowing;
            pauseMenuUI.SetActive(isShowing);
        }

        
    }


    public void UpdateBoolState()
    {
        // Updates the bool state if Resume button was pressed
        isShowing = !isShowing;

    }




}
