﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [HideInInspector] public bool facingRight = true;
   

    // Basic public  variables
    public float moveSpeed = 70f;
    public float jumpHeight = 100f;
    public float knockbackForce = 100f;
    public float knockbackRadius = 5f;


    // Jump variables
    private bool isGrounded = false;
    float groundCheckRadius = 1f;
    public LayerMask Ground;
    public Transform groundCheck;


    // UI variables
    public Text coinUpgradeText;
    public Text coinText;
    public int coinAmount;

    // Coin reward after killing an enemy
    private int coinIncrease;


    public Animator myAnim;
    public GameObject teddyObject;
    private Rigidbody2D teddyBody;

    // Use this for initialization
    void Awake()
    {
        myAnim = teddyObject.GetComponent<Animator>();
        teddyBody = teddyObject.GetComponent<Rigidbody2D>();

        coinAmount = 0;
        coinText.text = coinAmount.ToString();
        coinUpgradeText.text = coinAmount.ToString();

        coinIncrease = 50;
    }

    // Update is called once per frame
    void Update()
    {
        // Check if we are grounded if no then we are falling


        if (isGrounded && Input.GetAxis("Jump") > 0)
        {
            isGrounded = false;
            myAnim.SetBool("Grounded", isGrounded);
            teddyBody.AddForce(new Vector2(0f, jumpHeight)); // Using force to make the player jump
        }

        if (Input.GetKeyDown("space"))
        {
            // Increase coin amount upon enemy death
            coinAmount += coinIncrease;
            coinText.text = coinAmount.ToString();
            coinUpgradeText.text = coinAmount.ToString();
        }



    }

    // Physics code always goes in FixedUpdate
    void FixedUpdate()
    {
        // Check if we are Grounded if no then we are falling
        // Drawing a circel and checking if we intersect with the ground
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, Ground);
        myAnim.SetBool("Grounded", isGrounded);

        myAnim.SetFloat("verticalSpeed", teddyBody.velocity.y);

        // GetAxis returns a value between -1 and 1
        float move = Input.GetAxis("Horizontal"); // If A/D or arrow keys are pressed
        myAnim.SetFloat("Speed", Mathf.Abs(move));

        // Actually moving the rigidbody instead of using forces for moving
        teddyBody.velocity = new Vector2(move * moveSpeed, teddyBody.velocity.y);
      

        // Checking to see where the player is facing to flip accordingly
        if (moveSpeed > 0 && !facingRight)
        {
            Flip();
        }
        else if (moveSpeed < 0 && facingRight)
        {
            Flip();
        }
    }

    // Function to flip the character
    // Instead of having different animations for each side we just flip the whole Sprite
    void Flip()
    {
        facingRight = !facingRight;

        // teddyScale stands for the values of the scale on the object
        Vector3 teddyScale = transform.localScale;
        teddyScale.x *= -1;
        transform.localScale = teddyScale;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
		// Knockback effect using physics
        if (other.gameObject.tag == "Enemy")
        {
            this.teddyBody.AddForce(new Vector2(transform.position.x, 0f) * knockbackForce);
        }


        // Increase coin amount upon enemy death
        // coinAmount += coinIncrease;
        // coinText.text = coinAmount.ToString();
        // coinUpgradeText.text = coinAmount.ToString();


    }
}
