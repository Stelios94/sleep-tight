﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [HideInInspector] public bool facingRight = true;
   

    // Basic public  variables
    public float moveSpeed = 120f;
    public float crouchMoveSpeed = 75f;
    public float jumpHeight = 6000f;
    public float knockbackForce = 600f;
    public float knockbackRadius = 10f;

    // Jump variables
    private bool isGrounded = false; 
    private bool isFalling = false; // Needed for falling animation checks
    private bool hasLanded = false; // Needed to initiate the landing animation
    float groundCheckRadius = 10.0f;
    public LayerMask Ground;
    public Transform groundCheck;

    // Crouching variables
    private bool isCrouching = false;
    BoxCollider2D teddyCollider;
    Vector2 colliderSize;


    // Footstep Timer
    float footStepTimer = 0;


    // UI variables
    public Text coinUpgradeText;
    public Text coinText;
    public int coinTotal;


	// Player variables
	public int teddyHp;
	public int teddyDmg;


    // Coin reward after killing an enemy
    private int coinIncrease;

    // Basic variables for Teddy and Animator
    public Animator teddyAnim;
    public GameObject teddyObject;
    private Rigidbody2D teddyBody;
    public Transform spawnPoint;

    // For Footsteps!!!!!!!!!!!!!!!!!!!!!!!!
    public float teddySpeed;

    // Use this for initialization
    void Awake()
    {
        teddyAnim = teddyObject.GetComponent<Animator>();
        teddyBody = teddyObject.GetComponent<Rigidbody2D>();
        teddyCollider = transform.GetComponent<BoxCollider2D>(); // Needed for crouch movement
        colliderSize = GetComponent<BoxCollider2D>().size; // Needed for crouch movement

        // Setting the spawn point
        teddyBody.transform.position = new Vector2(spawnPoint.position.x, spawnPoint.position.y);
        

		// Setup the current currency and display it in the UI
        coinTotal = 0;
        coinText.text = coinTotal.ToString();
        coinUpgradeText.text = coinTotal.ToString();

		// Coin increase upon killing an enemy
        coinIncrease = 5;

		// Teddy base health and damage
		teddyHp = 5;
		teddyDmg = 1;
    }
    // Update is called once per frame
    void Update()
    {
        float airSpeed = Input.GetAxis("Jump"); // Variable to set the verticalSpeed in the animator

        // Check if we are grounded if no then we are falling
        if (isGrounded && Input.GetAxis("Jump") > 0)
        {
            isGrounded = false;
            teddyAnim.SetBool("Grounded", isGrounded);
            teddyBody.AddForce(new Vector2(0f, jumpHeight)); // Using force to make the player jump

            // Making sure the falliing animation plays
            isFalling = true;
            teddyAnim.SetBool("isFalling", isFalling);

            teddyAnim.SetFloat("verticalSpeed", Mathf.Abs(airSpeed));
        }
			
		// Placeholder to test coin increase
        if (Input.GetKeyDown("space"))
        {
            // Increase coin total upon enemy death
            coinTotal += coinIncrease;
        }


        // Updates the UI with current coin total
        coinText.text = coinTotal.ToString();
		coinUpgradeText.text = coinTotal.ToString();
    }

    // Physics code always goes in FixedUpdate
    void FixedUpdate()
    {
        // Check if we are Grounded if no then we are falling
        // Drawing a circle and checking if we intersect with the ground
        isGrounded = (Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, Ground) || Physics2D.Linecast(groundCheck.position, groundCheck.position + Vector3.down * groundCheckRadius * 2.0f, Ground)) && (teddyBody.velocity.y > -1.0f) && (teddyBody.velocity.y < 1.0f);
        teddyAnim.SetBool("Grounded", isGrounded);
        teddyAnim.SetBool("isFalling", isFalling);

        // Telling the animator that the Teddy is on the ground so all variables needed for the jumping and falling are reset
        if (isGrounded == true)
        {
            hasLanded = true;
            isFalling = false;
            teddyAnim.SetBool("teddyLanded", hasLanded);
            teddyAnim.SetBool("isFalling", isFalling);
        }


        // GetAxis returns a value between -1 and 1
        float move = Input.GetAxis("Horizontal"); // If A/D or arrow keys are pressed
        teddyAnim.SetFloat("Speed", Mathf.Abs(move));

        // Actually moving the rigidbody instead of using forces for moving
        teddyBody.velocity = new Vector2(move * moveSpeed, teddyBody.velocity.y);


        // For Footsteps!!!!!!!!!!!!!!!!!!!!!!!!
		teddySpeed = teddyBody.velocity.magnitude;
        
        //if (isGrounded && teddySpeed > 10f)
        //{
        //    if (footStepTimer > 0.30f)
        //    {
        //        InvokeRepeating("SendEventToWwise", 0f, 0f);
        //        footStepTimer = 0.0f;

        //    }

        //    footStepTimer += Time.deltaTime;
        
        //}

        


        // Crouching  and crouch movement
        if (Input.GetKeyDown(KeyCode.S))
        {
            isCrouching = true;
            teddyAnim.SetBool("Crouching", isCrouching);

        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            isCrouching = false;
            teddyAnim.SetBool("Crouching", isCrouching);
        }

        //Making the collider of the Teddy smaller when crouching
        if (isCrouching == true)
        {
            teddyCollider.size = new Vector2(teddyCollider.size.x, 0.7f);
        }
        else
        {
            teddyCollider.size = colliderSize;
        }

        // Crouch Movement
        if (teddyAnim.GetBool("Crouching") == true)
        {
            //Setting different movement speed when crouching
            float crouchMove = Input.GetAxis("Horizontal");
            teddyAnim.SetFloat("crouchSpeed", Mathf.Abs(crouchMove));

            teddyBody.velocity = new Vector2(crouchMove * crouchMoveSpeed, teddyBody.velocity.y);
        }

       

        // Checking to see where the player is facing to flip accordingly
        if (move > 0 && !facingRight)
        {
            Flip();
        }
        else if (move < 0 && facingRight)
        {
            Flip();
        }

     
    }

    // Function to flip the character
    // Instead of having different animations for each side we just flip the whole Sprite
    void Flip()
    {
        // Switches the labelled facing of the player
       facingRight = !facingRight;

       // teddyScale stands for the values of the scale on the object
       Vector3 teddyScale = transform.localScale;
       teddyScale.x *= -1;
       transform.localScale = teddyScale;
        
    }

    // For Footsteps!!!!!!!!!!!!!!!!!!!!!!!!

  //      void SendEventToWwise()
  //  {

		//print("Footstep");
		//AkSoundEngine.PostEvent ("Footstep", teddyObject);

  //  }



    void OnCollisionEnter2D(Collision2D other)
    {
		// Knockback effect using physics
        if (other.gameObject.tag == "Enemy")
        {
            teddyBody.AddForce(new Vector2(transform.position.x, 0f) * knockbackForce);
        }


        // Increase coin amount upon enemy death
        // coinTotal += coinIncrease;
        // coinText.text = coinTotal.ToString();
        // coinUpgradeText.text = coinTotal.ToString();


    }
}
