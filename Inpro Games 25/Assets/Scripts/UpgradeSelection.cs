﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeSelection : MonoBehaviour
{

    // Current state of the upgrade toggles
    bool hpState;
    bool dmgState;

    // Cost of the upgrades
    int hpCost;
    int dmgCost;

    // Increase amount for the upgrades
    int hpIncrease;
    int dmgIncrease;

    // Text box objects
    public Text hpDescription;
    public Text dmgDescription;
	public Text purchaseError;


	// Object for using player-related variables
    public PlayerController player;


    // Activates upon running program
    void Start()
    {
		// Upgrade variables
		hpCost = 5;
		dmgCost = 5;

        // Description with cost and increase for upgrades
        hpDescription.text = "Cost: " + hpCost.ToString() + "\n" + "Health Increase: " + hpIncrease.ToString();
        dmgDescription.text = "Cost: " + dmgCost.ToString() + "\n" + "Damage Increase: " + dmgIncrease.ToString();

    }


    // Changes the current state
    public void UpdateHpState()
    { 
        hpState = !hpState;
    }

    // Changes the current state
    public void UpdateDmgState()
    {
        dmgState = !dmgState;
    }


    // Cancels the selection of upgrades
    public void CancelSelection()
    {
		// Turns off the states
        hpState = false;
        dmgState = false;

		// Reset the purchase error
		purchaseError.text = "";
    }



    // Adds any selected upgrades to player as well as remove from total currency
    // Checks if the player has selected the option and if they can afford the upgrade
    public void PurchaseUpgrade()
    {
		
		// Reset the purchase error
		purchaseError.text = "";


        if (hpState == true)
        {

			// If player can afford the upgrade
			if (player.coinTotal >= hpCost) 
			{
				// Apply the upgrade
				player.coinTotal -= hpCost;
				player.teddyHp += hpIncrease;

			} 

			else 
			{
				// Not enough coins
				purchaseError.text = "Not enough buttons";
			}

			// Changes the state of the button to false
			hpState = false;



        } // End if


        if (dmgState == true)
        {

			// If player can afford the upgrade
            if (player.coinTotal >= dmgCost)
            {
				
				// Apply the upgrade
				player.coinTotal -= dmgCost;
				player.teddyDmg += dmgIncrease;
	
            }

			else 
			{
				// Not enough coins
				purchaseError.text = "Not enough buttons";
			}

			// Changes the state of the button to false
			dmgState = false;

        } // End if
			

	}	// End PurchaseUpgrade()


} // End Class
