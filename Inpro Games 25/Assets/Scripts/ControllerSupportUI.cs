﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControllerSupportUI : MonoBehaviour
{

    // ---- Controller Support ----

    // Variables for applying UI Controller support
    public EventSystem ES;
    private GameObject StoreSelected;

    // Objects for storing initial button selected (Controller Support)
    public GameObject controllerMain;
    public GameObject controllerPause;
    public GameObject controllerSettings;
    public GameObject controllerUpgrade;




    // Use this for initialization
    void Start()
    {
        // Sets selected object to First selected (defined in ES)
        StoreSelected = ES.firstSelectedGameObject;
    }




    // Update is called once per frame
    void Update()
    {

        // Forces UI option to be selected if mouse has clicked elsewhere (Controller Support)
        if (ES.currentSelectedGameObject != StoreSelected)
        {
            if (ES.currentSelectedGameObject == null)
            {
                ES.SetSelectedGameObject(StoreSelected);
            }
            else
            {
                StoreSelected = ES.currentSelectedGameObject;
            }
        }


    }


    // Functions to select each initial button

    public void mainController()
    {
        // Highlights the Play button for Controller
        ES.SetSelectedGameObject(controllerMain);
        StoreSelected = ES.currentSelectedGameObject;

    }


    public void pauseController()
    {
        // Highlights the Resume button for Controller
        ES.SetSelectedGameObject(controllerPause);
        StoreSelected = ES.currentSelectedGameObject;

    }


    public void settingsController()
    {
        // Highlights the music slider for Controller
        ES.SetSelectedGameObject(controllerSettings);
        StoreSelected = ES.currentSelectedGameObject;

    }


    public void upgradeController()
    {
   
        // Highlights the Purchase button for Controller
        ES.SetSelectedGameObject(controllerUpgrade);
        StoreSelected = ES.currentSelectedGameObject;

    }







}
