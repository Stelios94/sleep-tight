﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class UIController : MonoBehaviour
{


    // Objects for storing each UI element
    public GameObject mainMenuUI;
    public GameObject pauseMenuUI;
    public GameObject inGameUI;
    public GameObject settingsUI;
    public GameObject upgradeUI;

	// Level design and player object
    public GameObject Level1;
    public GameObject teddy;

	// Back button for the pause menu
	public GameObject backButtonMain;
	public GameObject backButtonGame;


    // Current visible state of the UI
    public bool isShowing;


    // Allows access to controller support functions and variables
    private ControllerSupportUI controller;


    // Use this for initialization
    void Start()
    {

        // Checks which scene is currently active to hide appropriate UI
        if (SceneManager.GetSceneByName("MainMenu") == SceneManager.GetActiveScene())
        {

            // Hides the UI
            pauseMenuUI.SetActive(isShowing);
            inGameUI.SetActive(isShowing);
            settingsUI.SetActive(isShowing);
            upgradeUI.SetActive(isShowing);

			// Hides Level and player
            Level1.SetActive(isShowing);
            teddy.SetActive(isShowing);

			// Hides the back button (In-game)
			backButtonGame.SetActive(isShowing);


        }
        else if (SceneManager.GetSceneByName("Level1") == SceneManager.GetActiveScene())
        {

            // Hides the UI
            mainMenuUI.SetActive(isShowing);
            pauseMenuUI.SetActive(isShowing);
            settingsUI.SetActive(isShowing);
            upgradeUI.SetActive(isShowing);

			// Hides the back button (Main)
			backButtonMain.SetActive(isShowing);

            
        }



    }

    // Update is called once per frame
    void Update()
    {


		// If the level scene is playing
		if (SceneManager.GetSceneByName ("Level1") == SceneManager.GetActiveScene ()) 
		{
			// If the user presses "esc" or "Start", toggle the pause menu UI
			if (Input.GetKeyDown ("escape") || Input.GetKeyDown ("joystick button 7")) 
			{
                // Update bool state and show pause menu
                UpdateBoolState();
				pauseMenuUI.SetActive (isShowing);

				// Highlights the Resume button for Controller
				controller.pauseController ();
			}

		} else 
		{
			// Wrong scene selected

		}

			
        
    }


    public void UpdateBoolState()
    {
        // Updates the bool state if Resume button was pressed
        isShowing = !isShowing;

    }




}
