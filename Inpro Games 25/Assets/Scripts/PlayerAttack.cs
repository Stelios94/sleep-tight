﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    // Variables for the Animator and the Teddy
    public Animator teddyAnim;
    public GameObject teddyObject;
    public Rigidbody2D teddyBody;

    // Layer Mask 
    public LayerMask Enemy;

    // Attack //
    private bool lightAttack = false;
    private bool heavyAttack = false;
    private bool enemyHit = false;
    float swordSwingDistance = 45.0f;

    // Line cast start points
    public Transform topCheck;
    public Transform middleCheck;
    public Transform bottomCheck;

	// Use this for initialization
	void Start ()
    {
        teddyAnim = teddyObject.GetComponent<Animator>();
        teddyBody = teddyObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Checking if the player pressed the attack button
        if (Input.GetMouseButtonDown(0))
        {
            // Setting the Animator bool to true to start the Attack Animation
            lightAttack = true;
            teddyAnim.SetBool("LightAttack", lightAttack);

            // Checking if the sword actually hit an enemy
            enemyHit = (Physics2D.Linecast(middleCheck.position, middleCheck.position + Vector3.right * swordSwingDistance * 20.0f, Enemy) ||Physics2D.Linecast(topCheck.position, topCheck.position + Vector3.right * swordSwingDistance * 20.0f, Enemy) || Physics2D.Linecast(bottomCheck.position, bottomCheck.position + Vector3.right * swordSwingDistance * 20.0f, Enemy));

            // Drawing the line that is cast on the previous line to be able to determine it in the editor
            Debug.DrawLine(topCheck.position, topCheck.position + Vector3.right * swordSwingDistance, Color.red);
            Debug.DrawLine(middleCheck.position, middleCheck.position + Vector3.right * swordSwingDistance, Color.red);
            Debug.DrawLine(bottomCheck.position, bottomCheck.position + Vector3.right * swordSwingDistance, Color.red);

            //if (enemyHit == true)
            //{

            //}
        }
        else if (Input.GetMouseButtonDown(1))
        {
            // Setting the Animator bool to true to start the Attack Animation
            heavyAttack = true;
            teddyAnim.SetBool("HeavyAttack", heavyAttack);

            // Checking if the sword actually hit an enemy
            enemyHit = (Physics2D.Linecast(middleCheck.position, middleCheck.position + Vector3.right * swordSwingDistance * 20.0f, Enemy) || Physics2D.Linecast(topCheck.position, topCheck.position + Vector3.right * swordSwingDistance * 20.0f, Enemy) || Physics2D.Linecast(bottomCheck.position, bottomCheck.position + Vector3.right * swordSwingDistance * 20.0f, Enemy));

            // Drawing the line that is cast on the previous line to be able to determine it in the editor
            Debug.DrawLine(topCheck.position, topCheck.position + Vector3.right * swordSwingDistance, Color.black);
            Debug.DrawLine(middleCheck.position, middleCheck.position + Vector3.right * swordSwingDistance, Color.black);
            Debug.DrawLine(bottomCheck.position, bottomCheck.position + Vector3.right * swordSwingDistance, Color.black);

        }
        else
        {
            lightAttack = false;
            teddyAnim.SetBool("LightAttack", lightAttack);

            heavyAttack = false;
            teddyAnim.SetBool("HeavyAttack", heavyAttack);
        }
	}
}
