﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    // Variables to use the trigger object fro wwise
    public GameObject soundTrigger;
    public Rigidbody triggerBody;

    // Variables in order to get footsteps
    public bool isMoving = false;
    public bool isStill = false;


    // Use this for initialization
    void Start ()
    {
        // Setting the triggerBody to be the object of the 3d object attached on the Teddy
        triggerBody = soundTrigger.GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void FixedUpdate()
    {
        if (triggerBody.velocity.x > 1.0f)
        {
            isMoving = true;
            isStill = false;
        }
        else
        {
            isStill = true;
            isMoving = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "StoneTrigger")
        {
            print("Footsteps");
            AkSoundEngine.PostEvent("Footstep", soundTrigger);
        }
    }
}
