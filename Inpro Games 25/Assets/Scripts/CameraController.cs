﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{
    // Camera Variables

    private Vector2 velocity;

    private float smoothTimeX;
    private float smoothTimeY;

    private GameObject player;

    private bool bounds;

    private Vector3 minCameraPos;
    private Vector3 maxCameraPos;



    
    void Start()
    {

        // Tracks player
        player = GameObject.FindGameObjectWithTag("Teddy");

        // Sets offset on camera follow
        smoothTimeX = 0.8f;
        smoothTimeY = 0.8f;

        bounds = true;

        // Sets boundaries on the camera
        minCameraPos = new Vector3(-525, 225, -250);
        maxCameraPos = new Vector3(4000, 500, -250);


    }


    void FixedUpdate()
    {
        float posX = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x, smoothTimeX);
        float posY = Mathf.SmoothDamp(transform.position.y, player.transform.position.y, ref velocity.y, smoothTimeY);


        transform.position = new Vector3(posX, posY, transform.position.z);


        if (bounds)
        {
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCameraPos.x, maxCameraPos.x),
                Mathf.Clamp(transform.position.y, minCameraPos.y, maxCameraPos.y),
                Mathf.Clamp(transform.position.z, minCameraPos.z, maxCameraPos.z));

        }



    }




}