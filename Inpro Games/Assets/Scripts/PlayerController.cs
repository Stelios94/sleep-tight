﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;

    // Basic public  variables
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;

    // Private Variables
    private bool isGrounded = false;
    private Animator myAnim;
    private Rigidbody2D myRB;

    // Use this for initialization
    void Awake()
    {
        myAnim = GetComponent<Animator>();
        myRB = GetComponent<Rigidbody2D>();
    }

	// Update is called once per frame
	void Update ()
    {
        // The only layer thnat we are gonna be casting is the Ground
        isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        // We can jump but not double jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            jump = true;
        }
		
	}

    // Physics code always goes in FixedUpdate
   void FixedUpdate()
    {
        // GetAxis returns a value between -1 and 1
        float h = Input.GetAxis("Horizontal"); // If A/D or arrow keys are pressed

        myAnim.SetFloat("Speed", Mathf.Abs(h)); // We use the positive float we get from the GetAxis to move

        // Applying forces to move the character
        if (h * myRB.velocity.x < maxSpeed)
        {
            myRB.AddForce(Vector2.right * h * moveForce);
        }

        // Check if we are going too fast and if so we clamp/set the velocity
        if (Mathf.Abs (myRB.velocity.x) > maxSpeed)
        {
            myRB.velocity = new Vector2(Mathf.Sign(myRB.velocity.x) * maxSpeed, myRB.velocity.y);
        }

        // Checking to see where the player is facing to flip accordingly
        if (h > 0 && !facingRight)
        {
            Flip();
        }
        else if (h < 0 && facingRight)
        {
            Flip();
        }

        if (jump)
        {
            myAnim.SetTrigger("Jump");
            myRB.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }

    // Function to flip the character
    // Instead of having different animations for each side we just flip the whole Sprite
    void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
